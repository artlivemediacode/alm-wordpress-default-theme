<?php
define( 'template_child_directory', dirname( get_bloginfo('stylesheet_url')));

function jupiter_child_enqueue_js() {
    //if (!is_admin()) {
        wp_register_script('custom_js', template_child_directory . '/assets/js/custom.js', array( 'jquery' ),false,true);
		wp_enqueue_script( 'custom_js' );
    //}
}
add_action('wp_enqueue_scripts', 'jupiter_child_enqueue_js');

function jupiter_styles()
{
    wp_register_style( 'styles_css', template_child_directory . '/assets/css/styles.min.css', array(), '1', 'all' );
    wp_enqueue_style( 'styles_css' );
}
add_action('wp_enqueue_scripts', 'jupiter_styles', 12);