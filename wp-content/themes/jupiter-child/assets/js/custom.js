var $ = jQuery.noConflict();
$(document).ready(function() {
	$(".home_r_work_nav").click(function () {
		if($(this).hasClass('left')) {
			$(".home_eat_drink .fusion-nav-prev").trigger("click");
		} else {
			$(".home_eat_drink .fusion-nav-next").trigger("click");
		}
	});

	$(".fusion-custom-overlay").click(function () {
		if($(this).data('link')) {
			window.location = $(this).data('link');
		}
	});

    $(".mobile_icon").click(function () {
        $(".mobilenav").fadeToggle(500);
        $(".top-menu").toggleClass("top-animate");
        $(".mid-menu").toggleClass("mid-animate");
        $(".bottom-menu").toggleClass("bottom-animate");
    });

    $(window).resize(function(){
	   console.log('resize called');
	   var width = $(window).width();
	   if(width <= 1078){
	       $('.fusion-header .fusion-row').removeClass('center-both');
	   }
	   else{
	       $('.fusion-header .fusion-row').addClass('center-both');
	   }
	})
	.resize();//trigger the resize event on page load.
});